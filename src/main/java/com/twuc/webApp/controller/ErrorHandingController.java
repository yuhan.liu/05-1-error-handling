package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@ControllerAdvice
public class ErrorHandingController {

    @GetMapping("/api/exceptions/runtime-exception")
    ResponseEntity<String> returnError() {
        throw new RuntimeException("run time!");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> solveError(RuntimeException exception){
        return ResponseEntity.status(404).body("handle exception");
    }

    @GetMapping("/api/exceptions/access-control-exception")
    public ResponseEntity<String> returnAccessError(){
        throw new AccessControlException("AccessControlException");
    }

    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<String> returnErrorOfExcept(AccessControlException exception){
        return ResponseEntity.status(403).body("access exception");
    }

    @GetMapping("/api/errors/null-pointer")
    public ResponseEntity<String> returnNullError(){
        throw new NullPointerException("NullPointerException");
    }

    @GetMapping("/api/errors/arithmetic")
    public ResponseEntity<String> returnArithmeticException(){
        throw new ArithmeticException("ArithmeticException");
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> returnTwoError(){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Something wrong with the argument");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> illegalArgumentException() {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .contentType(MediaType.APPLICATION_JSON)
                .body("Something wrong with brother or sister.");
    }
}
