package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ErrorHandingControllerTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void should_throw_error_with_runtime() throws Exception {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        String responseBody = forEntity.getBody();
        assertThat(responseBody).isEqualTo("handle exception");
    }

    @Test
    void should_throw_error_with__null_point() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/errors/null-pointer", String.class);
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
        assertThat(forEntity.getBody()).isEqualTo("Something wrong with the argument");
    }

    @Test
    void should_throw_error_with__arithmetic() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/arithmetic", String.class);
        assertThat( entity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
        assertThat(entity.getBody()).isEqualTo("Something wrong with the argument");
    }

    @Test
    void should_throw_error_when_brother_throw_illegal_argument_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
        assertThat(entity.getBody()).isEqualTo("Something wrong with brother or sister.");
    }

    @Test
    void should_throw_error_when_sister_throw_illegal_argument_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
        assertThat(entity.getBody()).isEqualTo("Something wrong with brother or sister.");
    }
}
